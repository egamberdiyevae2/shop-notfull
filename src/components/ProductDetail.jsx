import React, { useEffect, useState, useContext } from "react";
// import Product from "./Product";
import { useParams } from "react-router-dom";
import { useAxios } from "../hooks/useAxios";
// import { BasketContext } from "../Context/Context";
import { BasketContext } from "../App";

const ProductDetail = () => {
  const [data, setData] = useState({});
  const { res } = useAxios();
  const { id } = useParams();
  const { basket, setBasket } = useContext(BasketContext);
  useEffect(() => {
    setData(res[id - 1]);
  }, [res]);
  console.log(data);
  const addBasket = (item) => {
    if (basket.indexOf(item) !== -1) return;
    setBasket([...basket, item]);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <img src={data?.thumbnail} alt="" className="w-100" />
        </div>
        <div className="col-md-8 text-start">
          <h1> {data?.id + " " + data?.title}</h1>
          <p> {data?.description}</p>
          <p>
            <span className="text-danger"> Brand:</span> {data?.brand}
          </p>
          <p>
            <span className="text-danger"> Rating:</span> {data?.rating}
          </p>
          <p>
            <span className="text-danger"> Category:</span> {data?.category}
          </p>
          <h3>
            {data?.price}$
            <span className="text-danger px-5">
              {data?.discountPercentage}% OFF
            </span>
          </h3>

          <button
            className="btn btn-outline-warning btn-lg"
            onClick={() => addBasket(data)}
          >
            Add to card
          </button>

          <button className="btn btn-warning btn-lg ms-5">Buy now</button>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
