import React from "react";
import { useAxios } from "../hooks/useAxios";
import Cards from "../components/CategoryCards";
import Loader from "../components/Loader";

const Groceries = () => {
  const { res, loader } = useAxios("/products/category/groceries");
  return (
    <div>
      {loader ? (
        <Loader />
      ) : (
        res?.map(
          (data, id) => <Cards data={data} key={id} />
          // console.log(data.category)
        )
      )}
    </div>
  );
};

export default Groceries;
