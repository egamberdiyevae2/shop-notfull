import React from "react";
import { useAxios } from "../hooks/useAxios";
import Cards from "../components/CategoryCards";
import Loader from "../components/Loader";

const HomeDecoration = () => {
  const { res, loader } = useAxios("/products/category/home-decoration");
  return (
    <div>
      {loader ? (
        <Loader />
      ) : (
        res?.map((data, id) => <Cards data={data} key={id} />)
      )}
    </div>
  );
};

export default HomeDecoration;
