import axios from "axios";
import { useEffect, useState } from "react";
// import { useParams } from "react-router-dom";

axios.defaults.baseURL = "https://dummyjson.com";

export function useAxios(url) {
  const [err, setErr] = useState("");
  const [res, setRes] = useState([]);
  const [loader, setLoader] = useState(true);
  useEffect(() => {
    return async () => {
      try {
        const res = await axios.get(url);
        setRes(res.data.products);
      } catch (err) {
        setErr(err);
      } finally {
        setLoader(false);
      }
    };
  }, []);
  return { res, err, loader };
}

export default useAxios;
