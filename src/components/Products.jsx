import React from "react";
import Product from "./Product";
import { useAxios } from "../hooks/useAxios";
import Loader from "./Loader";

const Products = () => {
  const { res, loader } = useAxios("/products");

  return (
    <div className="row">
      {res?.map((data) => (
        <Product {...data} key={data.id} />
      ))}
      {loader && <Loader />}
    </div>
  );
};

export default Products;
