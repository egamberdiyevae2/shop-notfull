import React from "react";
import { useAxios } from "../hooks/useAxios";
import Cards from "../components/CategoryCards";
import Loader from "../components/Loader";

const Laptops = () => {
  const { res, loader } = useAxios("/products/category/laptops");
  return (
    <div>
      {loader ? (
        <Loader />
      ) : (
        res?.map(
          (data, id) => <Cards data={data} key={id} />
          // console.log(data.category)
        )
      )}
    </div>
  );
};

export default Laptops;
